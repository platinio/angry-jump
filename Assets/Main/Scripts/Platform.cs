﻿using UnityEngine;

namespace Platinio
{
    [RequireComponent( typeof( BoxCollider2D ) )]
    
    public class Platform : MonoBehaviour
    {
        [SerializeField] private bool isStackBase = false;

        private Vector2 size = Vector2.zero;
        private BoxCollider2D thisCollider = null;
        private float speed = 0.0f;
        private Vector2 dir = Vector2.zero;        
        private bool shouldMove = false;
        private MeshRenderer render = null;
        private int platformLayer = -1;
        private ColliderPusher[] colliderPushers = null;
        public ColliderPusher[] ColliderPushers { get { return colliderPushers; } }
        
        public Vector2 Size { get { return size; } }
        public bool IsVisible { get { return render.isVisible; } }
        public Vector2 TravelDirection { get { return dir; } }
        public Vector2 RayStartPosition { get { return transform.position; } }
        public Vector2 RayEndPosition { get { return RayStartPosition + ( Vector2.down * thisCollider.size.y ); } }
        public bool IsConnected
        {
            get
            {                
                RaycastHit2D[] hitArray = Physics2D.LinecastAll( RayStartPosition, RayEndPosition, platformLayer );

                for (int n = 0; n < hitArray.Length; n++)
                {
                    if (hitArray[n].collider != null && hitArray[n].collider.gameObject != gameObject)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private void Awake()
        {
            thisCollider = GetComponent<BoxCollider2D>();            
            render = GetComponent<MeshRenderer>();
            colliderPushers = gameObject.GetComponentsInChildren<ColliderPusher>();

            size = thisCollider.bounds.size;

            platformLayer = 1 << LayerMask.NameToLayer( "Platform" );
        }

        public void Construct(float speed , Vector2 dir, Color c)
        {
            this.speed = speed;
            this.dir = dir;
            shouldMove = true;

            render.material.color = c;
        }

        private void Update()
        {
            if(shouldMove)
                transform.position += (Vector3) dir * Time.deltaTime * speed;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!isStackBase)
            {               
                shouldMove = false;
            }
                
        }

    }

}

