﻿using UnityEngine;
using UnityEngine.Events;

namespace Platinio
{
    public class Coin : MonoBehaviour
    {
        [SerializeField] private float moveDistance = 0.0f;
        [SerializeField] private Transform follow = null;
        [SerializeField] private UnityEvent onCollected = null;

        private Collider2D thisCollider = null;
        private Platform basePlatform = null;

        public UnityEvent OnCollected { get { return onCollected; } }

        private void Awake()
        {
            basePlatform = FindObjectOfType<Platform>();

            thisCollider = GetComponent<Collider2D>();
            thisCollider.isTrigger = true;            
        }

        private void Start()
        {
            //set starting position
            transform.position = basePlatform.transform.position + ( Vector3.up * moveDistance );
        }

        private void Update()
        {
            transform.position = new Vector3( follow.position.x , transform.position.y , transform.position.z );
        }

        private void MoveCoin()
        {
            transform.position += Vector3.up * moveDistance;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                onCollected.Invoke();
                MoveCoin();
            }
        }

    }
}

