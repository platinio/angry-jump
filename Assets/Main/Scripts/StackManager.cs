﻿using UnityEngine;
using System.Collections;

namespace Platinio
{
    public class StackManager : MonoBehaviour
    {
       
        
        [SerializeField] private Transform platformsParent = null;

        private Platform basePlatform = null;
        private Coin coin = null;
        private Vector2 stackRootAproximatePosition = Vector2.zero;
        private int platformLayer = -1;
        private PlayerController player = null;

        public Vector2 StackRootAproximatePosition { get { return stackRootAproximatePosition; } }

        private void Awake()
        {
            player = FindObjectOfType<PlayerController>();
            basePlatform = FindObjectOfType<Platform>();
            coin = FindObjectOfType<Coin>();

            platformLayer = 1 << LayerMask.NameToLayer("Platform");
        }

        private void Start()
        {
            stackRootAproximatePosition = basePlatform.transform.position;

        }

        private void Update()
        {
            if (player.IsGrounded)
            {
                RaycastHit2D hit = Physics2D.Linecast(player.transform.position , (Vector2)player.transform.position + (Vector2.down * 5.0f) , platformLayer );

                if (hit.collider != null)
                {
                    stackRootAproximatePosition = (Vector2)hit.collider.transform.position + ( Vector2.up * hit.collider.bounds.size.y);
                }
            }
        }

        private void OnCollectedCoin()
        {
            //StartCoroutine( AlingPlatformsRoutine() );
        }
        /*
        private IEnumerator AlingPlatformsRoutine()
        {

            float posX = basePlatform.transform.position.x;

            Vector2 newPos;

            for (int n = 0; n < platformsParent.childCount; n++)
            {
                Platform p = platformsParent.GetChild( n ).GetComponent<Platform>();

                if (p.IsConnected)
                {
                    newPos = p.transform.position;
                    newPos.x = posX;
                    p.transform.position = newPos;
                }

                p.RB.velocity = Vector3.zero;

                yield return new WaitForEndOfFrame();
            }

            newPos = player.transform.position;
            newPos.x = posX;
            player.transform.position = newPos;

        }*/

       
    }
}

