﻿using UnityEngine;

namespace Platinio
{
    [RequireComponent( typeof( Rigidbody2D ) )]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float jumpForce = 150.0f;
        [SerializeField] private Vector2 groundCastOffset = Vector2.zero;
        [SerializeField] private float groundedRaycastLength = 0.2f;
        [SerializeField] private LayerMask groundedLayerMask = new LayerMask();
        
        private Rigidbody2D rb = null;
        private Collider2D thisCollider = null;
        
        public Vector2 GroundCastOrigin { get { return (Vector2) transform.position + groundCastOffset; } }
        public Vector2 GroundCastSize
        {
            get
            {
                if (thisCollider == null)
                    thisCollider = GetComponent<Collider2D>();

                return new Vector2( thisCollider.bounds.size.x , 0.1f );
            }
        }

        public bool IsGrounded
        {
            get
            {
                Vector2 startRay = transform.position;
                Vector2 endRay = (Vector2) transform.position + ( Vector2.down * groundedRaycastLength );
                
                return Physics2D.BoxCast(GroundCastOrigin , GroundCastSize, 0.0f , Vector2.down , groundedRaycastLength, groundedLayerMask.value);
            }
        }

        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            thisCollider = GetComponent<BoxCollider2D>();
        }

        private void Update()
        {
            if (CanJump())
                Jump();
        }

        private bool CanJump()
        {
            return IsGrounded && Input.GetKeyDown( KeyCode.Space );
        }

        private void Jump()
        {
            rb.AddForce(Vector2.up * jumpForce);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(GroundCastOrigin , new Vector2( GroundCastSize.x , GroundCastSize.y - 0.1f - groundedRaycastLength ));
        }
    }

}

