﻿using UnityEngine;

namespace Platinio
{
    public class PlatformCreator : MonoBehaviour
    {
        [SerializeField] private Platform platformPrefab = null;       
        [SerializeField] private Transform platformsParent = null;
        [SerializeField] private ColorManager colorManager = null;
        [SerializeField] private float platformVelocity = 0.5f;
        [SerializeField] private float distanceFromStack = 2.0f;
        [SerializeField] private float platformSpawnDelay = 0.5f;

        private StackManager stackManager = null;
        private GameObject player = null;
        private Platform currentPlatform = null;
        private bool shouldSpawnLeft = false;
        private float timer = 0.0f;        
        private int platformLayer = -1;

        private void Awake()
        {            
            stackManager = FindObjectOfType<StackManager>();

            player = GameObject.FindGameObjectWithTag("Player");
            platformLayer = 1 << LayerMask.NameToLayer( "Platform" );
        }

        private void Start()
        {
            SetColorForBasePlatform();
        }

        private void SetColorForBasePlatform()
        {
            FindObjectOfType<Platform>().GetComponent<MeshRenderer>().material.color = colorManager.CurrentColor;
            colorManager.UpdateLerp();
        }

        private void Update()
        {
            if (currentPlatform == null)
            {
                timer += Time.deltaTime;

                if (timer > platformSpawnDelay)
                {
                    SpawnAndSetupPlatform();
                    timer = 0.0f;
                }
                
            }
           
        }

        private void SpawnAndSetupPlatform()
        {
            currentPlatform = SpawnPlatform();
            currentPlatform.Construct( platformVelocity, CalculatePlatformDirection() , colorManager.CurrentColor);
            colorManager.UpdateLerp();
            SetupPlatformCollisionCallback( currentPlatform );            
        }

        private Platform SpawnPlatform()
        {
            shouldSpawnLeft = !shouldSpawnLeft;
            Platform p = Instantiate( platformPrefab, CalculatePlatformPosition(), Quaternion.identity );
            p.transform.parent = platformsParent;

            return p;
        }

        private void SetupPlatformCollisionCallback(Platform p)
        {
            OnCollisionListener listener = p.gameObject.AddComponent<OnCollisionListener>();
            listener.AddOnCollisionEnter2DListener( delegate (Collision2D other){ OnPlatformCollisionEnter( other , p ); } );
        }

        private void OnPlatformCollisionEnter(Collision2D other , Platform platform)
        {
            Destroy( platform.GetComponent<PlatformPositionCorrection>() );

            for (int n = 0; n < platform.ColliderPushers.Length; n++)
            {
                Destroy(platform.ColliderPushers[n]);
            }

            if (currentPlatform == platform)
                currentPlatform = null;
           
        }

        private bool PlatformWasAddToStack(Platform p)
        {
            if (player.transform.position.y < p.transform.position.y)
                return false;

            RaycastHit2D[] hitArray = Physics2D.BoxCastAll(p.transform.position , p.Size , 0.0f , Vector2.down, 5.0f , platformLayer);

            for (int n = 0; n < hitArray.Length; n++)
            {
                if (hitArray[n].collider.gameObject != p.gameObject)
                    return true;
            }

            return false;
        }

        private Vector2 CalculatePlatformPosition()
        {            
            return stackManager.StackRootAproximatePosition + ( shouldSpawnLeft ? Vector2.left : Vector2.right ) * distanceFromStack + ( Vector2.up * platformPrefab.Size.y / 2.0f);
        }

        private Vector2 CalculatePlatformDirection()
        {
            return shouldSpawnLeft ? Vector2.right : Vector2.left;
        }
    }

}

