﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platinio
{    
    public class GravityController : MonoBehaviour
    {
        [SerializeField] private float rot = 0.0f;
        [SerializeField] private float minAngle = 30.0f;
        [SerializeField] private float maxAngle = 300.0f;
        [SerializeField] private float maxGravitySpeed = 12.0f;

        private float gravitySpeed = 0.0f;
        private float speed = 0.0f;

        void Start()
        {
            speed = maxAngle / maxGravitySpeed;

            UpdateGravitySpeed();  
        }

        private void UpdateGravitySpeed()
        {
            gravitySpeed = rot / speed;
            gravitySpeed = Mathf.Clamp(gravitySpeed , -maxGravitySpeed , maxGravitySpeed);                       
            Physics2D.gravity = new Vector2( gravitySpeed, Physics2D.gravity.y );
        }


        void Update()
        {
            
            rot = Input.acceleration.x * 360.0f;

            //if we are in a normal range
            if (Mathf.Abs( rot ) < minAngle)
            {
                if (Physics2D.gravity.x != 0)
                {
                    Vector2 gravity = Physics2D.gravity;
                    gravity.x = 0;
                    Physics2D.gravity = gravity;
                }

                return;
            }

            UpdateGravitySpeed();
        }
    }

}

