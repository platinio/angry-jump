﻿using UnityEngine;

namespace Platinio
{
    [RequireComponent(typeof(Platform))]
    public class PlatformPositionCorrection : MonoBehaviour
    {
        [SerializeField] private Vector2 rayOffset = Vector2.zero;
        [SerializeField] private float rayLength = 0.0f;
        [SerializeField] private float velocity = 1.0f;

        private Platform platform = null;
        private int platformLayer = -1;

        public Vector2 RayStartPosition { get { return (Vector2) transform.position + rayOffset; } }
        public Vector2 RayEndPosition
        {
            get
            {
                if (platform == null)
                    platform = GetComponent<Platform>();

                return RayStartPosition + ( platform.TravelDirection * rayLength );
            }
        }
                
        private void Awake()
        {
            platform = GetComponent<Platform>();

            platformLayer =  1 << LayerMask.NameToLayer("Platform");
        }

        private void Update()
        {
            CorrectPosition();
        }

        private void CorrectPosition()
        {           
            RaycastHit2D[] hitArray = Physics2D.LinecastAll(RayStartPosition , RayEndPosition , platformLayer);

            for (int n = 0; n < hitArray.Length; n++)
            {
                if (hitArray[n].collider != null && hitArray[n].collider.gameObject != gameObject)
                {
                    transform.position += Vector3.up * velocity * Time.deltaTime;
                    return;
                }
            }
            
        }

        private void OnDrawGizmosSelected()
        {
            Vector2 rayEndPosition = RayStartPosition + ( Vector2.right * (rayLength) ) ;
            Gizmos.DrawLine(RayStartPosition , rayEndPosition);
        }
    }
}

