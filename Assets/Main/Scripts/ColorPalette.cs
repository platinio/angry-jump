﻿using UnityEngine;

namespace Platinio
{
    [CreateAssetMenu( menuName = "Platinio/Create Color Palette", fileName = "ColorPalette" )]
    public class ColorPalette : ScriptableObject
    {
        [SerializeField] private string colorPaletteName = null;
        [SerializeField] private Color[] colorArray = null;

        public Color[] ColorArray { get { return colorArray; } }
    }
}

