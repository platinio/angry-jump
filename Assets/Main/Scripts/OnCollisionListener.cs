﻿using UnityEngine;
using System;

namespace Platinio
{
    public class OnCollisionListener : MonoBehaviour
    {
        private Action<Collision2D> onCollisionEnter2D = null;

        public void AddOnCollisionEnter2DListener(Action<Collision2D> callback)
        {
            onCollisionEnter2D += callback;
        }

        public void RemoveOnCollisionEnter2DListener(Action<Collision2D> callback)
        {
            onCollisionEnter2D -= callback;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            onCollisionEnter2D(other);
        }
    }
}

