﻿using UnityEngine;
using System;

namespace Platinio
{
    public class OnDestroyListener : MonoBehaviour
    {
        private Action OnDestroyCallback = null;

        public void AddOnDestroyCallback(Action callback)
        {
            OnDestroyCallback += callback;
        }

        public void RemoveOnDestroyCallback(Action callback)
        {
            OnDestroyCallback -= callback;
        }

        private void OnDestroy()
        {
            if(OnDestroyCallback != null)
                OnDestroyCallback();
        }
    }
}

