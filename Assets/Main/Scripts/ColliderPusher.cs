﻿using UnityEngine;

namespace Platinio
{
    public class ColliderPusher : MonoBehaviour
    {
        [SerializeField] private float pushForce = 50.0f;

        private void Awake()
        {
            Collider2D col = GetComponent<Collider2D>();

            if (col != null)
                col.isTrigger = true;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
                rb.velocity = Vector2.zero;
                Vector2 dir = (other.transform.position - transform.position);
                dir.y = 0.0f;
                dir.Normalize();
                rb.AddForce(dir * pushForce);

            }
        }
    }
}

