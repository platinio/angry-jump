﻿using UnityEngine;

namespace Platinio
{
    public class ColorManager : MonoBehaviour
    {
        [SerializeField] private ColorPalette[] colorPaletteArray = null;
        [SerializeField] private float lerpSpeed = 0.1f;

        private ColorPalette currentColorPallete = null;
        private Color currentColor = new Color();
        private float currentLerpValue = 0.0f;
        private int colorPaletteIndex = 0;

        public Color CurrentColor { get { return currentColor; } }

        private void Awake()
        {
            SetupRandomColorPalette();
        }

        private void SetupRandomColorPalette()
        {
            currentColorPallete = PickRandomColorPalette();
            currentColor = currentColorPallete.ColorArray[0];
            colorPaletteIndex = 1;
        }

        public void UpdateLerp()
        {
            currentLerpValue += lerpSpeed;

            Color from = currentColorPallete.ColorArray[colorPaletteIndex - 1];
            Color to = currentColorPallete.ColorArray[colorPaletteIndex + 1 >= currentColorPallete.ColorArray.Length ? 0 : colorPaletteIndex];

            currentColor = Color.Lerp( from , to , currentLerpValue );
           
            if (currentLerpValue > 1.0f)
            {
                currentLerpValue = 0.0f;
                colorPaletteIndex++;

                if (colorPaletteIndex >= currentColorPallete.ColorArray.Length)
                {
                    SetupRandomColorPalette();
                }
            }
        }

        private ColorPalette PickRandomColorPalette()
        {
            return colorPaletteArray[Random.Range(0 , colorPaletteArray.Length)];
        }
    }

}

